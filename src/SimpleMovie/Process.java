import java.util.*;
import java.sql.*;
import java.io.*;
import java.text.*;
import java.util.Date;

public class Process {
	
	private String filename;
	private String tablename;
	private String JDBC_Driver = "com.mysql.jdbc.Driver";
	private String DB_URL = "";
	private String USER = "";
	private String PASS = "";
	private String charSet = "";
	private String defaultTime = "";
	private String dateFormat = "";
	private String dateFormat2 = "";
	private String dateFormat3 = "";
	private String nullTime	= "";
	private Connection conn = null;
	private Statement  stmt = null;
	private ArrayList<movieData>  list;

	public int dbInit() throws Exception {
		try {
			Class.forName(JDBC_Driver);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(true);
			stmt = conn.createStatement();
			String sql = "CREATE TABLE " + tablename + " (MOVIE_NAME VARCHAR(50), DIRECTOR VARCHAR(50), ACTOR VARCHAR(50), GENERE VARCHAR(50), TIME DATE)";
			stmt.executeUpdate(sql);

		}catch(SQLException se){
      		//Handle errors for JDBC
      		se.printStackTrace();
   		}catch(Exception e){
      		//Handle errors for Class.forName
      		e.printStackTrace();
   		}finally{
      		//finally block used to close resources
      		try{
         		if(stmt!=null)
            		stmt.close();
      		}catch(SQLException se2){
      		}// nothing we can do
      		try{
         		if(conn!=null)
            		conn.close();
      		}catch(SQLException se){
         		se.printStackTrace();
      		}//end finally try
   		}

   		return 0;
	}

	public boolean checkTableExist(String tableName) throws Exception {
		Class.forName(JDBC_Driver);
		Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
		ResultSet tabs = null;
		try {
			DatabaseMetaData dbMetaData = conn.getMetaData();
			String[]   types   =   { "TABLE" };
			tabs = dbMetaData.getTables(null, null, tableName, types);
			if (tabs.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			tabs.close();
			conn.close();
		}
		return false;
	}

	public int store() {
		try {
			Class.forName(JDBC_Driver);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			while(list.size() > 0) {
				movieData line = list.remove(0);
				String[] datalist = line.datalist;
				Date date = line.movie_date;
				String sql = "INSERT INTO movie (MOVIE_NAME, DIRECTOR, ACTOR, GENERE, TIME) VALUES ('"+datalist[0]+"', '"+datalist[1]+"', '"+datalist[2]+"', '"+datalist[3]+"', '"+sdf.format(date.getTime())+"')";
				//System.out.println(sql);
				PreparedStatement insertStmt = conn.prepareStatement(sql);
				insertStmt.executeUpdate(sql);
			}
		}catch(SQLException se){
      		//Handle errors for JDBC
      		se.printStackTrace();
   		}catch(Exception e){
      		//Handle errors for Class.forName
      		e.printStackTrace();
   		}finally{
      		//finally block used to close resources
      		try{
         		if(stmt!=null)
            		stmt.close();
      		}catch(SQLException se2){
      		}// nothing we can do
      		try{
         		if(conn!=null)
            		conn.close();
      		}catch(SQLException se){
         		se.printStackTrace();
      		}//end finally try
   		}

   		return 0;
	}

	public int setting(ArrayList<String> settings) throws Exception {
		if (settings.size() < 9)
			return -1;
		else {
			filename 	= settings.get(0);
			DB_URL 		= settings.get(1); 
			tablename	= settings.get(2);
			JDBC_Driver = settings.get(3);
			USER		= settings.get(4);
			PASS		= settings.get(5);
			charSet		= settings.get(6);
			defaultTime	= settings.get(7);
			dateFormat 	= settings.get(8);
			dateFormat2 = settings.get(9);
			dateFormat3 = settings.get(10);
			nullTime	= settings.get(11);
			list 		= new ArrayList<movieData>();
			try {
				if (!checkTableExist(tablename)) {
					int rc = dbInit();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}
	}

	public int process() {
		
		FileInputStream file = null;
		BufferedReader in = null;

		int count = 0;

		try {

			file = new FileInputStream(filename);
			in = new BufferedReader(new InputStreamReader(file, charSet));
			String line = in.readLine();
			while(line != null) {
				count++;
				String[] parts = line.split("\t");
				Date movieDate = null;
				if (parts[4].startsWith(nullTime)) {
					parts[4] = defaultTime;
				}	
				SimpleDateFormat format = new SimpleDateFormat(dateFormat);
				try {
					movieDate = format.parse(parts[4]);
				} catch (ParseException px) {
					SimpleDateFormat format2 = new SimpleDateFormat(dateFormat2);
					try {
						movieDate = format2.parse(parts[4]);
					} catch (Exception ex) {
						SimpleDateFormat format3 = new SimpleDateFormat(dateFormat3);
						try {
							movieDate = format3.parse(parts[4]);
						} catch (Exception ex2) {
							ex2.printStackTrace();
						}	
					}
				}
				list.add(new movieData(parts, movieDate));
				line = in.readLine();
			}
		
        } catch (IOException ex) {
        	System.out.println("IOException");
        	return -1;
        } finally {
        	//System.out.println("OtherException");
            try {
                file.close();
                in.close();
                //return -1;
            } catch (IOException ex) {
            }
        }
        int rc = store();
		return 0;
	}

}