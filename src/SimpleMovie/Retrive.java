import java.util.*;
import java.sql.*;
import java.text.*;
import java.util.Date;

public class Retrive {
	
	private String filename;
	private String tablename;
	private String JDBC_Driver = "com.mysql.jdbc.Driver";
	private String DB_URL = "";
	private String USER = "";
	private String PASS = "";
	private String charSet = "";
	private String defaultTime = "";
	private String dateFormat = "";
	private String dateFormat2 = "";
	private String dateFormat3 = "";
	private String nullTime	= "";
	private Connection conn = null;
	private Statement  stmt = null;

	public ArrayList<movieData> GetByOneField(String fieldname, String value) {
		
		ArrayList<movieData> resultList = new ArrayList<movieData>();
		
		try {
			Class.forName(JDBC_Driver);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String sql = "SELECT * FROM movie WHERE " + fieldname + " LIKE '%" + value + "%';";
			System.out.println(sql);
			ResultSet resultSet = stmt.executeQuery(sql);
			while (resultSet.next()) {
				String[] datalist_ = new String[4];
				datalist_[0] = resultSet.getString("MOVIE_NAME");
				//System.out.println(datalist_[0]);
				datalist_[1]	= resultSet.getString("DIRECTOR");
				datalist_[2]	= resultSet.getString("ACTOR");
				datalist_[3]	= resultSet.getString("GENERE");
				Date date_			= sdf.parse(resultSet.getString("TIME"));
				movieData md_ = new movieData(datalist_, date_);
				resultList.add(md_);
			}
		}catch(SQLException se){
      		//Handle errors for JDBC
      		se.printStackTrace();
   		}catch(Exception e){
      		//Handle errors for Class.forName
      		e.printStackTrace();
   		}finally{
      		//finally block used to close resources
      		try{
         		if(stmt!=null)
            		stmt.close();
      		}catch(SQLException se2){
      		}// nothing we can do
      		try{
         		if(conn!=null)
            		conn.close();
      		}catch(SQLException se){
         		se.printStackTrace();
      		}//end finally try
   		}
   		//System.out.println(resultList.size());
   		return resultList;
	}

	public ArrayList<movieData> GetByMultipleField(ArrayList<String[]> queryList) {
		
		String sql = "SELECT * FROM movie WHERE " + queryList.get(0)[0] + " LIKE '%" + queryList.get(0)[1] + "%'";
		for (int i = 1; i < queryList.size(); i++) {
			sql += " AND " + queryList.get(i)[0] + " LIKE '%" + queryList.get(i)[1] + "%'";
		}

		ArrayList<movieData> resultList = new ArrayList<movieData>();
		
		try {
			Class.forName(JDBC_Driver);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			System.out.println(sql);
			ResultSet resultSet = stmt.executeQuery(sql);
			while (resultSet.next()) {
				String[] datalist_ = new String[4];
				datalist_[0] = resultSet.getString("MOVIE_NAME");
				//System.out.println(datalist_[0]);
				datalist_[1]	= resultSet.getString("DIRECTOR");
				datalist_[2]	= resultSet.getString("ACTOR");
				datalist_[3]	= resultSet.getString("GENERE");
				Date date_			= sdf.parse(resultSet.getString("TIME"));
				movieData md_ = new movieData(datalist_, date_);
				resultList.add(md_);
			}
		}catch(SQLException se){
      		//Handle errors for JDBC
      		se.printStackTrace();
   		}catch(Exception e){
      		//Handle errors for Class.forName
      		e.printStackTrace();
   		}finally{
      		//finally block used to close resources
      		try{
         		if(stmt!=null)
            		stmt.close();
      		}catch(SQLException se2){
      		}// nothing we can do
      		try{
         		if(conn!=null)
            		conn.close();
      		}catch(SQLException se){
         		se.printStackTrace();
      		}//end finally try
   		}
   		//System.out.println(resultList.size());
   		return resultList;
	}

	public int setting(ArrayList<String> settings) throws Exception {
		if (settings.size() < 9)
			return -1;
		else {
			filename 	= settings.get(0);
			DB_URL 		= settings.get(1); 
			tablename	= settings.get(2);
			JDBC_Driver = settings.get(3);
			USER		= settings.get(4);
			PASS		= settings.get(5);
			charSet		= settings.get(6);
			defaultTime	= settings.get(7);
			dateFormat 	= settings.get(8);
			dateFormat2 = settings.get(9);
			dateFormat3 = settings.get(10);
			nullTime	= settings.get(11);
			return 0;
		}
	}
}