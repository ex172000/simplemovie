import java.util.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.*;
import java.io.BufferedReader;
import java.util.logging.*;

public class SimpleMovie {

	public static void main(String args[]) {
		String filename 	= "mtime.txt";
		String DBURL		= "jdbc:mysql://10.18.26.150/ali?useUnicode=true&characterEncoding=gbk";
		String tablename	= "movie";
		String driver		= "com.mysql.jdbc.Driver";
		String username		= "root";
		String password		= "19910423";
		String charSet 		= "GB18030";
		String defaultTime	= "1970年1月1日";
		String dateFormat	= "yyyy年MM月dd日";
		String dateFormat2	= "yyyy年MM月";
		String dateFormat3	= "yyyy年";
		String nullTime		= "No";
		
		ArrayList<String> settingList = new ArrayList<>();
		settingList.add(filename);
		settingList.add(DBURL);
		settingList.add(tablename);
		settingList.add(driver);
		settingList.add(username);
		settingList.add(password);
		settingList.add(charSet);
		settingList.add(defaultTime);
		settingList.add(dateFormat);
		settingList.add(dateFormat2);
		settingList.add(dateFormat3);
		settingList.add(nullTime);

		Retrive rt = new Retrive();
		try {
			int rc = rt.setting(settingList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ArrayList<String[]> querymap = new ArrayList<>();
		for (int i = 0; i < args.length; i++) {
			switch(args[i]) {
				case "-l":
					Process proc = new Process();
					try {
						int rc = proc.setting(settingList);
					} catch (Exception e) {
						e.printStackTrace();
					}
					proc.process();
					break;
				case "-m":
					String[] m = new String[2];
					m[0] = "MOVIE_NAME";
					m[1] = args[i+1];
					querymap.add(m);
					break;
				case "-d":
					String[] d = new String[2];
					d[0] = "DIRECTOR";
					d[1] = args[i+1];
					querymap.add(d);
					break;
				case "-a":
					String[] a = new String[2];
					a[0] = "ACTOR";
					a[1] = args[i+1];
					querymap.add(a);
					break;
				case "-g":
					String[] g = new String[2];
					g[0] = "GENERE";
					g[1] = args[i+1];
					querymap.add(g);
					break;
				case "-y":
					String[] y = new String[2];
					y[0] = "TIME";
					y[1] = args[i+1];
					querymap.add(y);
					break;
				default:
			}
		}

		PrintResult(rt.GetByMultipleField(querymap));

	}

	public static void PrintResult(ArrayList<movieData> movie_data) {
		int count = 0;
		while(movie_data.size() > 0) {
			count++;
			movieData md_ = movie_data.remove(0);
			System.out.print(count + "\t\t" + md_.datalist[0] + ' ' + md_.datalist[1] + ' ' + md_.datalist[2] + ' ' + md_.datalist[3] + ' ' + md_.movie_date + '\n');
		}
		System.out.println("Successfully Retrived " + count + " Records!");
	}
}